package com.shiku.redisson;

@FunctionalInterface
public interface LockCallBack<T> {

    Object execute(T t);
}
