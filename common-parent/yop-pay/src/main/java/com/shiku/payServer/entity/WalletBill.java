package com.shiku.payServer.entity;

import lombok.Data;

/**
 * @author zhm
 * @version V1.0
 * @Description: TODO(记录)
 * @date 2019/10/29 19:10
 */
@Data
public class WalletBill {

    // 商户请求号
    private String requestNo;

    // 易宝订单号
    private String businessNo;

    // 订单状态
    private String orderStatus;

    // 下单时间
    private String orderTime;

    // 完成时间
    private String completeTime;

    // 用户手续费金额
    private String userFee;

}
