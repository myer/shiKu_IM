package com.shiku.payServer.payServer;

import com.alibaba.fastjson.JSONObject;
import com.shiku.payServer.YopPayConfig;
import com.shiku.payServer.utils.YopPayUtil;
import com.yeepay.g3.sdk.yop.client.YopRequest;
import com.yeepay.g3.sdk.yop.client.YopResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;

/**
 * @author zhm
 * @version V1.0
 * @Description: TODO(易宝支付接口调用服务)
 * @date 2019/10/11 15:47
 */
@Slf4j
@Service
public class PayServer {
    @Autowired(required = false)
    private YopPayConfig yopPayConfig;

    YopRequest yopRequest = null;

    /**
     *  开户
     * @param userId 用户唯一标识
     * @param name 真实姓名
     * @param certificateNo 身份证号
     * @param mobile 手机号
     *  SUCCESS 返回结果说明
     *  "businessNo" : "a982517e59064b09a4647a2321251f0d",  易宝订单号
     *  "walletUserNo" : "211434798356",  钱包账户ID
     *  "walletCategory" : "ONE_CATEGORY"  钱包账户等级
     */
    public JSONObject openAccount(int userId,String name, String certificateNo, String mobile){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("name",name); // 姓名
        yopRequest.addParam("certificateType","IDENTITY_CARD"); // 证件类型
        yopRequest.addParam("certificateNo",certificateNo); // 证件号码
        yopRequest.addParam("requestNo",YopPayUtil.getOutTradeNo());
        yopRequest.addParam("merchantUserNo",userId);
        yopRequest.addParam("mobile",mobile);// 手机号	非必传
        JSONObject object = null;
        try {
            YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.OPENACCOUNT,yopRequest);
            log.info("调用返回结果 "+response.toString());
            if("SUCCESS".equals(response.getState())){
                object = JSONObject.parseObject(response.getStringResult());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return object;
    }

    /**
     *  查询钱包账户信息
     *  SUCCESS 返回内容说明
     * "    walletUserNo" : "211434798356",  钱包账户ID
     *     "walletStatus" : "AVAILABLY",  钱包账户状态   AVAILABLY：正常  FREEZE：冻结  CANCAEL：已注销
     *     "balance" : "0.00",  钱包账户余额
     *     "name" : "郑慧敏",  姓名
     *     "certificateType" : "IDENTITY_CARD",  证件类型  IDENTITY_CARD：身份证
     *     "certificateNo" : "420521*******37",  证件号码
     *     "walletCategory" : "ONE_CATEGORY",  钱包账户等级
     *     "tradePswdSet" : false  是否已设置交易密码
     */
    public JSONObject queryAccount(int userId){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("merchantUserNo",userId);
        JSONObject object = null;
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.QUERYACCOUNT,yopRequest);
        log.info("调用返回结果 "+response.toString());
        object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     *  查询绑定银行卡
     *  SUCCESS 返回值说明
     *  "url" : "https://member.yeepay.com/yeepay-std-wallet"  绑定银行卡URL
     */
    public JSONObject queryCard(int userId){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("redirectUrl",yopPayConfig.getRedirectUrl());
        yopRequest.addParam("requestNo",YopPayUtil.getOutTradeNo());
        yopRequest.addParam("merchantUserNo",userId);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.QUERYCARD,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     *  安全设置（用于用户修改或重新设置钱包支付密码）
     *  SUCCESS 返回值说明
     *  "url" : "https://member.yeepay.com/yeepay-std-wallet/operatePwd.html"  安全设置URL （设置密码界面）
     */
    public JSONObject managePassword(int userId){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("redirectUrl",yopPayConfig.getRedirectUrl());
        yopRequest.addParam("requestNo",YopPayUtil.getOutTradeNo());
        yopRequest.addParam("merchantUserNo",userId);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.MANAGEPASSWORD,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     *  发起充值
     *  SUCCESS 返回值说明
     *  "url" : "https://member.yeepay.com/yeepay-std-wallet/operateMoney.html"  交易支付确认控件URL（输密码界面）
     *  "requestNo":""  自己生成的订单号
     */
    public JSONObject recharge(int userId,String money,String tradeNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("amount",money); // 充值金额
        yopRequest.addParam("redirectUrl",yopPayConfig.getRedirectUrl());// 页面重定向地址
        yopRequest.addParam("notifyUrl",yopPayConfig.getCallBackUrl().getRecharge());// 服务器回调地址
        yopRequest.addParam("requestNo",tradeNo);
        yopRequest.addParam("merchantUserNo",userId);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.RECHARGE,yopRequest);
        log.info("调用返回结果 "+response.toString()+" 请求订单号 "+yopRequest.getParam("requestNo").get(0));
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        object.put("requestNo",yopRequest.getParam("requestNo").get(0));
        return object;
    }

    /**
     * 充值查询
     * @param orderNo 自己生成的订单号
     * @return
     *  SUCCESS 返回值说明
     *  "requestNo":"",  // 商户请求号
     *  "businessNo":"", // 易宝订单号
     *  "amount":"", // 充值金额
     *  "bindId":"", // 绑卡ID
     *  "cardNo":"", // 卡号
     *  "bankName":"", // 银行名称
     *  "bankCode":"", // 银行编码
     *  "orderStatus":"", // 订单状态	SUCCESS：充值成功 FAIL：充值失败
     *  "orderTime":"", // 订单完成时间
     *  "userFee":"", // 用户手续费
     */
    public JSONObject queryRecharge(String orderNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("requestNo",orderNo);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.QUERYRECHARGE,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     *  一对一发送红包
     *  SUCCESS 返回值说明
     *  "redirectUrl":"", // 支付收银台控件URL
     *  "redPacketNo":"", // 红包ID
     */
    public JSONObject sendRedPacket(String sendUserNo,String receiveUserNo,String money,String tradeNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("requestNo",tradeNo);
        yopRequest.addParam("sendUserNo",sendUserNo);// 用户在商户端唯一编号
        yopRequest.addParam("receiveUserNo",receiveUserNo);// 用户在商户端唯一编号
        yopRequest.addParam("amount",money);// 金额
        yopRequest.addParam("notifyUrl",yopPayConfig.getCallBackUrl().getSendRedPacket());// 服务器回调地址
        yopRequest.addParam("redirectUrl",yopPayConfig.getRedirectUrl());// 页面回调地址
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.SENDREDPACKET,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 普通群红包发送
     * SUCCESS 返回结果说明
     *  "redirectUrl":"", // 支付收银台控件URL
     *  "redPacketNo":"", // 红包ID
     */
    public JSONObject sendGroupRedPacket(String sendUserNo, String money, int count,String tradeNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("requestNo",tradeNo);
        yopRequest.addParam("sendUserNo",sendUserNo);// 用户在商户端唯一编号
        yopRequest.addParam("singleAmount",money);// 红包金额
        yopRequest.addParam("count",count);// 红包个数
        yopRequest.addParam("notifyUrl",yopPayConfig.getCallBackUrl().getSendRedPacket());// 发红包成功后的服务器回调地址
        yopRequest.addParam("redirectUrl",yopPayConfig.getRedirectUrl());// 页面回调地址
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.GROUPREDPACKET,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 拼手气红包
     * @param sendUserNo 商户用户ID
     * @param money 金额
     * @param count 数量
     * @return
     * SUCCESS 返回结果说明
     * "redirectUrl":"", 支付收银台控件URL
     * "redPacketNo":""  红包ID
     */
    public JSONObject sendRandomRedPacket(String sendUserNo, String money,int count,String tradeNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("requestNo",tradeNo);
        yopRequest.addParam("sendUserNo",sendUserNo);// 用户在商户端唯一编号
        yopRequest.addParam("amount",money);// 红包金额
        yopRequest.addParam("count",count);// 红包个数
        yopRequest.addParam("notifyUrl",yopPayConfig.getCallBackUrl().getSendRedPacket());// 发红包成功后的服务器回调地址
        yopRequest.addParam("redirectUrl",yopPayConfig.getRedirectUrl());// 页面回调地址
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.RANDOMREDPACKET,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     *  抢红包（类似查询红包状态）
     * @param redPacketNo 红包Id
     * @param grabUserNo 商户用户ID
     * @return JSONObject
     * SUCCESS 返回结果说明
     * "grabResult":"", 抢红包结果	OPENED(已领取), CAN_OPEN(可领取),EMPTY(红包已空,此用户没抢到) ,EXPIRE(过期或红包不存在)
     */
    public JSONObject grabRedPacket(String redPacketNo,String grabUserNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("redPacketNo",redPacketNo);
        yopRequest.addParam("grabUserNo",grabUserNo);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.GRABREDPACKET,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     *  拆红包
     * @param  redPacketNo 红包ID
     * @param  receiveUserNo 商户用户ID
     * @return JSONObject
     * SUCCESS 返回结果说明
     * "openResult":"", 抢红包结果 OK(拆红包成功), EXPIRE(红包过期或不存在), NONE(红包已经领光), RECEIVED(此用户)
     * "amount":"", 金额 拆到的红包金额
     */
    public JSONObject openRedPacket(String redPacketNo,String receiveUserNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("redPacketNo",redPacketNo);
        yopRequest.addParam("receiveUserNo",receiveUserNo);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.OPENREDPACKET,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 红包支付结果查询
     * @param redPacketNo 红包ID
     * @return JSONObject
     * SUCCESS 返回结果说明
     * "status":"", 支付结果  INIT:未付，SUCCESS:发红包成功
     * "payWay":""  支付方式  BALANCE:余额支付 BIND_CARD:绑卡支付
     */
    public JSONObject queryRedPacketPayment(String redPacketNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("redPacketNo",redPacketNo);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.QUERYREDPACKETPAYMENT,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 红包信息查询
     * @param redPacketNo  红包ID
     * @return JSONObject
     * SUCCESS 返回结果说明
     * "sendUserNo":"", 红包发送者用户编号
     * "amount":"", 红包总金额
     * "receiveAmount","", 总领取金额
     * "count":"", 总数量
     * "receiveCount":"", 总领取数量
     * "redPacketCreateTime":"", 红包发送时间
     * "redPacketExpireTime":"", 红包过期时间
     * "receivedList":"" 子红包列表(用户领取详情)
     *      "receiveUserNo":"",  红包领取用户ID
     *      "amount":"", 红包金额
     *      "receiveTime":"" 领取时间
     *      .......
     */
    public JSONObject queryRedPacketInfo(String redPacketNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("redPacketNo",redPacketNo);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.QUERYREDPACKETINFO,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 发起转账
     * @param targetMerchantUserNo 转账接收商户用户ID
     * @param merchantUserNo 转账发起商户用户ID
     * @param money 转账金额
     * @return JSONObject
     * SUCCESS 返回结果说明
     *  "url":"" // 交易支付确认控件URL
     */
    public JSONObject transfer(String merchantUserNo,String targetMerchantUserNo,String money,String tradeNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("targetMerchantUserNo",targetMerchantUserNo);// 转账接收商户用户Id
        yopRequest.addParam("amount",money);// 转账金额
        yopRequest.addParam("redirectUrl",yopPayConfig.getRedirectUrl());// 页面重定向地址
        yopRequest.addParam("notifyUrl",yopPayConfig.getCallBackUrl().getTransfer());// 服务器回调地址
        yopRequest.addParam("requestNo",tradeNo);// 转账
        yopRequest.addParam("merchantUserNo",merchantUserNo);// 转账发起商户用户ID
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.TRANSFER,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;

    }

    /**
     * 接收转账
     * @param requestNo 转账流水号（订单号）
     * @return JSONObject
     * SUCCESS 返回结果说明
     *  "requestNo":"", 商户请求号
     *  "businessNo":"", 易宝订单号
     *  "amount":"", 转账金额
     *  "receivedTime":"", 转账接收时间
     *  "tradeStatus":"" 交易状态 SUCCESS：转账成功  RECEIVE_TIMEOUT：转账过期已退还
     */
    public JSONObject receiveTransfer(String requestNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("requestNo",requestNo);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.RECEIVETRANSFER,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 转账退回（转账拒收）
     * 用户在商户端发起转账拒收，商户调用此接口退回该笔转账。
     * @param requestNo 商户请求号(订单号)
     * @return JSONObject
     * SUCCESS 返回结果说明
     * "requestNo":"", 商户请求号
     * "businessNo":"", 易宝订单号
     * "amount":"", 转账金额
     * "rejectTime":"", 拒收时间
     * "tradeStatus":"" 交易状态 REJECTED：对方拒收已退还
     */
    public JSONObject TransferReject(String requestNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("requestNo",requestNo);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.REJECTTRANSFER,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 转账查询
     * 商户通过请求该接口查询用户转账信息
     * @param requestNo 商户请求号(订单号)
     * @return
     * SUCCESS 返回结果说明
     * "requestNo":"", 商户请求号
     * "businessNo":"", 易宝订单号
     * "amount":"", 转账金额
     * "transferType":"", 转账方式	BALANCE：余额 CARD：银行卡
     * "bindId":"", 绑卡ID
     * "cardNo":"", 卡号
     * "bankName":"", 银行名称
     * "bankCode":"", 银行编码
     * "transferTime":"", 转账转出时间
     * "receiveTime":"", 转账接收时间
     * "rejectTime":"", 拒收时间
     * "tradeStatus":"", 交易状态 WAITING_RECEPTION:转账待确认接收   FAIL:转账失败  SUCCESS：转账成功   RECEIVE_TIMEOUT：转账过期已退还 REJECTED：对方拒收已退还
     * "userFee":"" 用户手续费
     */
    public JSONObject queryTransfer(String requestNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("requestNo",requestNo);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.QUERYTRANSFER,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 提现
     * @param money 提现金额
     * @return
     * SUCCESS 返回结果说明
     * "url":"" 支付密码确认控件URL
     */
    public JSONObject withdraw(int userId,String money,String tradeNo,int withdrawType){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("amount",money);
        yopRequest.addParam("withdrawType",withdrawType);// 提现类型
        yopRequest.addParam("redirectUrl",yopPayConfig.getRedirectUrl());// 页面重定向地址
        yopRequest.addParam("notifyUrl",yopPayConfig.getCallBackUrl().getWithdraw());// 服务器回调地址
        yopRequest.addParam("requestNo",tradeNo);
        yopRequest.addParam("merchantUserNo",userId);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.WITHDRAW,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 提现查询
     * @param requestNo 商户请求号(订单号)
     * @return
     * SUCCESS 返回结果说明
     * "requestNo":"", 商户请求号
     * "businessNo":"", 易宝订单号
     * "amount":"", 提现金额
     * "bindId":"", 绑卡ID
     * "cardNo":"", 卡号
     * "bankName":"", 银行名称
     * "bankCode":"", 银行编码
     * "tradeStatus":"", 交易状态 ACCEPTED:提现已受理 SUCCESS:提现已到账 FAIL:提现失败
     * "acceptTime":"", 提现受理时间
     * "completeTime":"", 提现到账时间
     * "userFee":"", 用户手续费
     * "withdrawType":"" 提现类型	0：实时到账（如果不传默认走实时到账） 1：2小时到账 2：次日
     */
    public JSONObject queryWithdraw(String requestNo){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("requestNo",requestNo);
        YopResponse response = YopPayUtil.request(YopPayUtil.PayUrl.QUERYWITHDRAW,yopRequest);
        log.info("调用返回结果 "+response.toString());
        JSONObject object = JSONObject.parseObject(response.getStringResult());
        return object;
    }

    /**
     * 钱包账单下载
     * 请求成功返回的是一个数据流，把数据流处理后进行对账。获取对账文件时请不要以表头长度来截取对账文件信息，
     * 后期可能会新增表头参数，每天凌晨定时生成前一天的对账文件，故请商户9:00 之后下载前一天的对账文件
     *
     * @param billDate 账单日期
     * @param bizType 账单类型
     * @param url 输出路径
     * @return
     */
    public void downloadBill(String billDate,String bizType,String url){
        yopRequest = new YopRequest(yopPayConfig.getAppKey(),yopPayConfig.getPrivate_key());
        yopRequest.addParam("billDate",billDate);
        yopRequest.addParam("bizType",bizType);
        YopResponse response = YopPayUtil.requestGet(YopPayUtil.PayUrl.DOWNLOADBILL,yopRequest);
        log.info("调用返回结果 "+response.toString());
        InputStream retstream= response.getFile();
        BufferedInputStream bi = new BufferedInputStream(retstream);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(bi));
        try {
            FileOutputStream fos = new FileOutputStream(url);
            byte[] by = new byte[1024];
            int bytesWritten = 0;
            int len = 0;
            while((len=bufferedReader.read())!=-1){
                fos.write(by,bytesWritten,len);
                bytesWritten +=len;
            }
            fos.close();
            bufferedReader.close();
        }catch (Exception e){
            e.printStackTrace();
        }
//        try {
//            OutputStream outputStream = new FileOutputStream(url);
//            byte[] by = new byte[1024];
//            int len = 0;
//            int bytesWritten = 0;
//            while ((len = retstream.read(by))!= -1){
//                outputStream.write(by,0,len);
//                bytesWritten +=len;
//            }
//            retstream.close();
//            outputStream.close();
//        }catch (Exception e){
//            e.printStackTrace();
//        }

    }
}
