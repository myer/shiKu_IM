package com.shiku.payServer.utils;

import com.yeepay.g3.sdk.yop.client.YopRequest;
import com.yeepay.g3.sdk.yop.client.YopResponse;
import com.yeepay.g3.sdk.yop.client.YopRsaClient;

/**
 * @author zhm
 * @version V1.0
 * @Description: TODO(易宝支付工具类)
 * @date 2019/10/11 15:49
 */
public class YopPayUtil {

    // 接口请求地址
    public interface PayUrl{
        // 开立钱包账户
        public static final String OPENACCOUNT = "/rest/v1.0/wallet/account/open";
        // 查询钱包账户信息
        public static final String QUERYACCOUNT = "/rest/v1.0/wallet/account/query";
        // 查询绑定银行卡
        public static final String QUERYCARD = "/rest/v1.0/wallet/card/query";
        // 安全设置
        public static final String MANAGEPASSWORD = "/rest/v1.0/wallet/password/manage";
        // 发起充值
        public static final String RECHARGE = "/rest/v1.0/wallet/recharge/initiate";
        // 充值查询
        public static final String QUERYRECHARGE = "/rest/v1.0/wallet/recharge/query";
        // 一对一红包
        public static final String SENDREDPACKET = "/rest/v1.0/wallet/redpacket/single/send";
        // 普通群红包
        public static final String GROUPREDPACKET = "/rest/v1.0/wallet/redpacket/normal/send";
        // 拼手气群红包
        public static final String RANDOMREDPACKET ="/rest/v1.0/wallet/redpacket/random/send";
        // 抢红包
        public static final String GRABREDPACKET = "/rest/v1.0/wallet/redpacket/grab";
        // 拆红包
        public static final String OPENREDPACKET = "/rest/v1.0/wallet/redpacket/open";
        // 红包支付结果查询
        public static final String QUERYREDPACKETPAYMENT = "/rest/v1.0/wallet/redpacket/payment/query";
        // 红包信息查询
        public static final String QUERYREDPACKETINFO = "/rest/v1.0/wallet/redpacket/receive/list";
        // 发起转账
        public static final String TRANSFER = "/rest/v1.0/wallet/transfer/initiate";
        // 转账接收
        public static final String RECEIVETRANSFER = "/rest/v1.0/wallet/transfer/receive";
        // 转账拒绝
        public static final String REJECTTRANSFER = "/rest/v1.0/wallet/transfer/reject";
        // 转账查询
        public static final String QUERYTRANSFER = "/rest/v1.0/wallet/transfer/query";
        // 发起提现
        public static final String WITHDRAW = "/rest/v1.0/wallet/withdraw/initiate";
        // 提现查询
        public static final String QUERYWITHDRAW = "/rest/v1.0/wallet/withdraw/query";
        // 钱包账单下载
        public static final String DOWNLOADBILL = "/yos/v1.0/wallet/bill/download";
    }

    public static YopResponse request(String url, YopRequest yopRequest){
        try {
            YopResponse response = YopRsaClient.post(url,yopRequest);
            if("FAILURE".equals(response.getState())){
                System.out.println("错误码: "+response.getError().getCode());
                System.out.println("错误原因: "+response.getError().getMessage());
                System.out.println("子错误: "+response.getError().getSubCode());
                System.out.println("子错误原因: "+response.getError().getSubMessage());
            }
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static YopResponse requestGet(String url,YopRequest yopRequest){
        try {
            YopResponse response = YopRsaClient.get(url,yopRequest);
            if("FAILURE".equals(response.getState())){
                System.out.println("错误码: "+response.getError().getCode());
                System.out.println("错误原因: "+response.getError().getMessage());
                System.out.println("子错误: "+response.getError().getSubCode());
                System.out.println("子错误原因: "+response.getError().getSubMessage());
            }
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    // 生成订单号
    public static String getOutTradeNo() {
        int r1 = (int) (Math.random() * (10));// 产生2个0-9的随机数
        int r2 = (int) (Math.random() * (10));
        long now = System.currentTimeMillis();// 一个13位的时间戳
        String id = String.valueOf(r1) + String.valueOf(r2)
                + String.valueOf(now);// 订单ID
        return id;
    }

}
