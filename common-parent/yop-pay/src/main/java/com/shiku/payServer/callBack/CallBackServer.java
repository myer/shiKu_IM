package com.shiku.payServer.callBack;

import com.alibaba.fastjson.JSONObject;
import com.shiku.payServer.YopPayConfig;
import com.yeepay.g3.sdk.yop.encrypt.DigitalEnvelopeDTO;
import com.yeepay.g3.sdk.yop.utils.DigitalEnvelopeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * @author zhm
 * @version V1.0
 * @Description: TODO(异步回调处理)
 * @date 2019/10/11 15:50
 */
public abstract class CallBackServer {
    @Autowired
    private YopPayConfig yopPayConfig;
    /**
     * 异步通知
     * @param
     * @return
     */
    public JSONObject CallBack(HttpServletRequest request, HttpServletResponse response){
        String responseMsg = request.getParameter("response");
        DigitalEnvelopeDTO digitalEnvelopeDTO = new DigitalEnvelopeDTO();
        digitalEnvelopeDTO.setCipherText(responseMsg);
        try {
            //设置商户私钥
            PrivateKey privateKey = getPrivateKey(yopPayConfig.getPrivate_key());
            //设置易宝公钥
            PublicKey publicKey = getPublicKey(yopPayConfig.getYBPublicKey());
            //解密验签
            digitalEnvelopeDTO = DigitalEnvelopeUtils.decrypt(digitalEnvelopeDTO,privateKey,publicKey);
            // 打印回调数据
            System.out.println(digitalEnvelopeDTO.getPlainText());

        }catch (Exception e){
            e.printStackTrace();
        }
        return JSONObject.parseObject(digitalEnvelopeDTO.getPlainText());
    }

    /**
     * String 转换为publickey
     * @param key
     * @return
     * @throws Exception
     */
    private static PublicKey getPublicKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = (new BASE64Decoder()).decodeBuffer(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return publicKey;
    }

    /**
     * String转私钥PrivateKey
     * @param key
     * @return
     * @throws Exception
     */
    private static PrivateKey getPrivateKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = (new BASE64Decoder()).decodeBuffer(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }
}
