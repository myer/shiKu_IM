package com.shiku.payServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhm
 * @version V1.0
 * @Description: TODO(todo)
 * @date 2019/10/30 17:35
 */
@Configuration
@EnableConfigurationProperties(YopPayConfig.class)
public class YopPayAutoConfiguration {
    @Autowired
    private YopPayConfig yopPayConfig;
}
