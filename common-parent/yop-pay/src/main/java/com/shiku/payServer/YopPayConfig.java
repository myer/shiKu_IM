package com.shiku.payServer;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhm
 * @version V1.0
 * @Description: TODO(todo)
 * @date 2019/10/29 14:49
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "yoppayconfig")
public class YopPayConfig {

    // APPID
    private String appId;
    // APPKEY
    private String appKey;
    // 商户编号
    private String merchantID;
    // 应用私钥
    private String private_key;
    // 平台公钥
    private String YBPublicKey;
    // 输入密码支付后跳转地址
    private String redirectUrl;

    private CallBackUrl callBackUrl;

    // 异步通知地址
    @Data
    public static class CallBackUrl{
        // 充值异步通知
        private String recharge;
        // 发送红包异步通知
        private String sendRedPacket;
        // 转账异步通知
        private String transfer;
        // 提现异步通知
        private String withdraw;
    }
}
