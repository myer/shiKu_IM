package com.shiku.im.admin.dao.impl;

import com.shiku.common.model.PageResult;
import com.shiku.im.admin.dao.UrlManagerDao;
import com.shiku.im.admin.entity.UrlManager;
import com.shiku.im.comm.utils.StringUtil;

import com.shiku.im.repository.MongoRepository;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *     外链地址
 * </p>
 *
 * @Author xx
 * @Date 2020/12/31
 **/
@Repository
public class UrlManagerDaoImpl extends MongoRepository<UrlManager, ObjectId> implements UrlManagerDao {


    @Override
    public MongoTemplate getDatastore() {
        return super.getDatastore();
    }

    @Override
    public void addUrlManager(UrlManager urlManager) {
        getDatastore().save(urlManager);
    }

    @Override
    public void updateUrlManager(ObjectId id, Map<String, Object> map) {
        Query query = createQuery("_id",id);
        Update ops = createUpdate();
        map.forEach((key,value)->{
            ops.set(key,value);
        });
        getDatastore().updateFirst(query,ops,getEntityClass());
    }

    @Override
    public void deleteUrlManager(ObjectId id) {
        deleteById(id);
    }


    @Override
    public PageResult<UrlManager> getUrlManagerList(int pageIndex, int pageSize,String keyword) {
        Query query = createQuery();
        if(0 != pageSize){
            query.with(createPageRequest(pageIndex,pageSize));
        }
        if(!StringUtil.isEmpty(keyword)){
            query.addCriteria(Criteria.where("name").regex(".*" + keyword + ".*"));
        }
        descByquery(query,"createTime");
        PageResult<UrlManager> result = new PageResult<>();
        result.setData(getDatastore().find(query,getEntityClass()));
        result.setCount(count(query));
        return result;
    }

    @Override
    public UrlManager getUrlManager(ObjectId id) {
        Query query = createQuery("_id",id);
        return findOne(query);
    }

    @Override
    public List<UrlManager> getList() {
        Query query = createQuery();
        query.addCriteria(Criteria.where("status").is(1));
        descByquery(query,"createTime");
        List<UrlManager>  result = getDatastore().find(query,getEntityClass());
        return result;
    }

    @Override
    public Class<UrlManager> getEntityClass() {
        return UrlManager.class;
    }
}
