package com.shiku.im.admin.service.impl;

import com.shiku.common.model.PageResult;
import com.shiku.im.admin.dao.UrlManagerDao;
import com.shiku.im.admin.entity.UrlManager;
import com.shiku.im.admin.service.UrlManagerService;
import com.shiku.im.comm.utils.StringUtil;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UrlManagerServiceImpl implements UrlManagerService {


    @Autowired
    private UrlManagerDao urlManagerDao;

    @Override
    public void addUrlManager(UrlManager urlManager) {

        urlManagerDao.addUrlManager(urlManager);

    }

    @Override
    public void updateUrlManager(ObjectId id, UrlManager urlManager) {

        Map<String,Object> map = new HashMap<>();
        if(!StringUtil.isEmpty(urlManager.getUrl())){
            map.put("url",urlManager.getUrl());
        }
        map.put("status",urlManager.getStatus());
        if(!StringUtil.isEmpty(urlManager.getName())){
            map.put("name",urlManager.getName());
        }
        if(!StringUtil.isEmpty(urlManager.getIcon())){
            map.put("icon",urlManager.getIcon());
        }
        urlManagerDao.updateUrlManager(id,map);

    }

    @Override
    public void deleteUrlManager(ObjectId id) {
        urlManagerDao.deleteUrlManager(id);
    }

    @Override
    public PageResult<UrlManager> getUrlManagerList(int pageIndex, int pageSize, String keyword) {
        return urlManagerDao.getUrlManagerList(pageIndex,pageSize,keyword);
    }

    @Override
    public UrlManager getUrlManager(ObjectId id) {
        return urlManagerDao.getUrlManager(id);
    }

    @Override
    public List<UrlManager> getList() {
        return urlManagerDao.getList();
    }
}
