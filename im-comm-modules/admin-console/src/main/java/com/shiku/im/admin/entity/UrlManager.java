package com.shiku.im.admin.entity;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * <p>
 *    外链路径
 * </p>
 *
 * @Author xx
 * @Date 2020/12/31
 **/
@Data
@Document(value = "urlManager")
public class UrlManager {
    @Id
    private ObjectId id;

    private String url;// 外链路径

    private String icon;// 外链图标路径

    private String name;// 外链名称

    private int status;// 外链状态：0:关闭，1：开启

    private long createTime;

}