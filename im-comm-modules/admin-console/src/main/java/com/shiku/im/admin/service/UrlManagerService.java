package com.shiku.im.admin.service;

import com.shiku.common.model.PageResult;
import com.shiku.im.admin.entity.UrlManager;
import org.bson.types.ObjectId;

import java.util.List;

public interface UrlManagerService {

    void addUrlManager(UrlManager urlManager);

    void updateUrlManager(ObjectId id, UrlManager urlManager);

    void deleteUrlManager(ObjectId id);

    PageResult<UrlManager> getUrlManagerList(int pageIndex, int pageSize, String keyword);

    UrlManager getUrlManager(ObjectId id);

    List<UrlManager> getList();
}
