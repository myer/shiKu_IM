package com.shiku.im.admin.dao;

import com.shiku.common.model.PageResult;
import com.shiku.im.admin.entity.UrlManager;
import com.shiku.im.repository.IMongoDAO;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Map;

public interface UrlManagerDao extends IMongoDAO<UrlManager, ObjectId> {

    void  addUrlManager(UrlManager urlManager);

    void updateUrlManager(ObjectId id, Map<String,Object> map);

    void deleteUrlManager(ObjectId id);

    PageResult<UrlManager> getUrlManagerList(int pageIndex, int pageSize, String keyword);

    UrlManager getUrlManager(ObjectId id);

    List<UrlManager> getList();
}