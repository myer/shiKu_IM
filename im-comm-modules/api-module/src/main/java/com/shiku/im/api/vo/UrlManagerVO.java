package com.shiku.im.api.vo;

import lombok.Data;

@Data
public class UrlManagerVO {

    private String url;// 外链路径

    private String icon;// 外链图标路径

    private String name;// 外链名称
}
