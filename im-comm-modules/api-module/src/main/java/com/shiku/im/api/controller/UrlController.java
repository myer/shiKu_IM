package com.shiku.im.api.controller;

import com.shiku.im.admin.entity.UrlManager;
import com.shiku.im.admin.service.impl.UrlManagerServiceImpl;
import com.shiku.im.api.vo.UrlManagerVO;
import com.shiku.im.comm.utils.BeanUtils;
import com.shiku.im.vo.JSONMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *       外链控制
 * </p>
 *
 * @Author xx
 * @Date 2021/1/4
 **/
@Api(value = "urlController" , tags = "外链操作")
@RestController
@RequestMapping(value = "/url", method={RequestMethod.GET,RequestMethod.POST})
public class UrlController {

    @Autowired
    private UrlManagerServiceImpl urlManagerService;

    /** @Description: 外链列表
     * @return
     **/
    @ApiOperation("外链列表")
    @RequestMapping(value = "/list")
    public JSONMessage getRegisterList() {
        List<UrlManager> data=urlManagerService.getList();
        List<UrlManagerVO> list = new ArrayList<>();
        for(UrlManager tmp:data){
            UrlManagerVO urlManagerVO = new UrlManagerVO();
            BeanUtils.copyProperties(tmp,urlManagerVO);
            list.add(urlManagerVO);
        }
        return JSONMessage.success(list);
    }
}
