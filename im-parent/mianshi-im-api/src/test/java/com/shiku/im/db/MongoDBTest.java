package com.shiku.im.db;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.shiku.im.comm.utils.DateUtil;
import com.shiku.im.company.dao.DepartmentDao;
import com.shiku.im.company.entity.Employee;
import com.shiku.im.pay.service.impl.ConsumeRecordManagerImpl;
import com.shiku.im.pay.utils.PayPassword;
import com.shiku.im.user.dao.UserDao;
import com.shiku.im.user.dao.impl.UserDaoImpl;
import com.shiku.im.user.entity.UserStatusCount;
import com.shiku.im.utils.SKBeanUtils;
// import com.shiku.im.yop.entity.YopWalletBill;
// import com.shiku.im.yop.service.impl.YopWalletBillServiceImpl;
import com.shiku.mianshi.Application;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= Application.class)
public class MongoDBTest {

	@Autowired
	private DepartmentDao departmentDao;
	// @Autowired
	// private YopWalletBillServiceImpl yopWalletBillService;
	@Autowired
	private ConsumeRecordManagerImpl consumeRecordManager;

	@Test
	public void testAddressDB() {
		//SKBeanUtils.getCompanyManager().createCompany("广州科技有限公司", 54123);
		Employee employee = new Employee();
		employee.setUserId(10000050);
		employee.setNickname("变形金刚");
		employee.setDepartmentId(new ObjectId("5d8a0473514f773e080a6ec8"));
		employee.setCompanyId(new ObjectId("5d8a0473514f773e080a6ec4"));
		employee.setRole((byte) 0);
		employee.setPosition("普通员工");
		employee.setChatNum(0);
		employee.setIsPause(1);
		employee.setIsCustomer(0);
		employee.setOperationType(1);

//		SKBeanUtils.getEmployeeDaoManager().addEmployee(employee);
		/*Department Customer = new Department();
		Customer.setCompanyId(new ObjectId("5d8a0473514f773e080a6ec4"));
		Customer.setParentId(new ObjectId("5d905faf514f77372436a294"));  //ParentId 为根部门的id
		Customer.setDepartName("研发部");
		Customer.setCreateUserId(54123); //创建者即公司的创建者
		Customer.setCreateTime(DateUtil.currentTimeSeconds());
		Customer.setEmpNum(0);
		Customer.setType(1);
		departmentDao.addDepartment(Customer); //添加部门记录*/
	}


	@Test
	public void testRoomFenBiao() {
		/*Integer userId=10004251;
		ObjectId id = new ObjectId();
		System.out.println(userId+"_"+id.getCounter());*/

//		User user = SKBeanUtils.getUserManager().getUser(10000048);
//		System.out.println("user = " + user.toString());
	}
	@Test
	public void testDBObject() {
		DBObject dbObject=new BasicDBObject("aa",111);
//		MongoCollection collection = SKBeanUtils.getLocalSpringBeanManager().getMongoClient().getDatabase("test1").getCollection("test",DBObject.class);
//		collection.insertOne(dbObject);
	}


	@Test
	public void updateUserPayPassword(){
		System.out.println(PayPassword.encodeFromOldPassword("10019280","e10adc3949ba59abbe56e057f20f883e"));
	}

	@Test
	public void getUserMoney(){
//		Double monet = yopWalletBillService.getUserYopRedPacktMoney(10002382,1, "2019-11-18 00:00:00" ,"2019-11-18 23:20:47");
//		System.out.println("用户充值金额 "+monet);
	}

	@Test
	public void getConsumRecord(){
		System.out.println(DateUtil.getTodayMorning());
		System.out.println(DateUtil.getTodayNight());
		System.out.println(DateUtil.TimeToStr(DateUtil.getTodayMorning()));
		System.out.println(DateUtil.TimeToStr(DateUtil.getTodayNight()));
		Double money = consumeRecordManager.getUserPayMoney(10008945,10,1,DateUtil.getTodayMorning().getTime()/1000 ,DateUtil.getTodayNight().getTime()/1000);
		System.out.println("用户交易金额 "+money);



	}

	// @Test
	// public void getUserFirstRecharge(){
	// 	YopWalletBill yopWalletBill = yopWalletBillService.getUserFirstRecharge(10002382);
	// 	System.out.println(yopWalletBill.toString());
	// }


@Resource
UserDaoImpl userDaoImpl;


    @Test
    public void testMongodb() {
        //1.建立连接
        MongoClient mongoClient = new MongoClient("192.168.0.100", 27017);
        //2.连接到数据库
        MongoDatabase mongoDatabase = mongoClient.getDatabase("test");
        //3.获取集合
        MongoCollection<Document> collection = mongoDatabase.getCollection("user");


        //创建文档
        Document document = new Document("name", "aaaa")
                .append("sex", 1)
                .append("age", 10);
        //插入一个文档
        // collection.insertOne(document);
        // 插入多个 ：  collection.insertMany();
		// collection.mapReduce()

		// System.out.println(userDaoImpl.getCollectionName(UserStatusCount.class));

	}








}
