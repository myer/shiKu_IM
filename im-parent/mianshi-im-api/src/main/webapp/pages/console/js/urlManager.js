layui.use(['form', 'layer', 'laydate', 'table', 'laytpl'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;

    //非管理员登录屏蔽操作按钮
    if (localStorage.getItem("IS_ADMIN") == 0) {
        $(".bindingSDK_div").empty();
    }
    // 接收者账户列表
    var tableIns = table.render({
        elem: '#receiveAccount_table'
        , url: request("/console/urlList")
        , page: true
        , curr: 0
        , limit: Common.limit
        , limits: Common.limits
        , groups: 7
        , cols: [[ //表头
            {field: 'id', title: '外链Id', sort: true, width: 250}
            , {field: 'name', title: '外链名称', sort: true, width: 120}
            , {field: 'url', title: '外链地址', sort: true, width: 450}
            , {field: 'icon', title: '外链图标', sort: true,  width: 306, templet:'<div><img style="height:35px;width:35px;" src="{{d.icon}}"></div>'}
            , {field: 'status', title: '外链状态', sort: true, width: 120,templet: function (item) {
                      switch (item.status) {
                          case 0:
                              return '关闭';
                              break;
                          case 1:
                              return '开启';
                              break;
                          default:
                              return '未定义';
                              break;
                      }
                }}
            , {fixed: 'right', width: 150, title: "操作", align: 'left', toolbar: '#receiveAccountBar'}
        ]]
        , done: function (res, curr, count) {
            checkRequst(res);

        }
    });

    // 表格操作
    table.on('tool(receiveAccount_table)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        console.log(data);
        if (layEvent === 'delete') {
            UrlManager.deleteReceiveAccount(data.id);
        } else if (layEvent === 'updateAccountInfo') {
            UrlManager.updateReceiveAccount(data);
        }
    })

    //搜索
    $(".search_receiveAccount").on("click", function () {
        table.reload("receiveAccount_table", {
            url: request("/console/urlList"),
            where: {
                keyword: Common.getValueForElement(".receiveAccount_keyword")
            },
            page: {
                curr: 1 //重新从第 1 页开始
            }
        })
        $(".receiveAccount_keyword").val("");
    })

    // 添加外链
    $(".add_receiveAccount_btn").on("click", function () {
        $("#updateReceiveAccountId").val("");
        $("#receiveAccountList").hide();
        $("#AddreceiveAccount").show();
        $("#name").val("");
        $("#url").val("");
        $("#receiveCodeImg_update").html("");
        $("#uploadreceiveCodeImg").attr("action", Config.getConfig().uploadUrl + "/upload/UploadServlet");
        $("#icon_tr").show();
        $("#url_tr").show();
    })

})

var UrlManager = {

    selectUpdateCodeMsg: function () {
        $("#uploadCodeImg").click();
    },
    uploadCodeMsg: function () {
        var uploadCoverFile = $("#uploadCodeImg")[0].files[0];
        $("#uploadreceiveCodeImg").ajaxSubmit(function (data) {
            var obj = eval("(" + data + ")");
            $("#receiveCodeImg_update").html(Common.filterHtmlData(obj.data.images[0].oUrl));
            $("#receiveCodeImg_update").show();
        });
    },
    commit_addReceiveAccount: function () {
        if (!Common.isNil($("#updateReceiveAccountId").val())) {
            Common.invoke({
                url: request('/console/updateUrl'),
                data: {
                    id: Common.getValueForElement("#updateReceiveAccountId"),
                    icon: Common.filterHtmlData($("#receiveCodeImg_update").html()),
                    type: Common.getValueForElement("#add_Account_type"),
                    name: Common.getValueForElement("#name"),
                    url: Common.getValueForElement("#url"),
                    status: $('input[name="status"]:checked').val()
                },
                success: function (result) {
                    if (result.resultCode == 1) {
                        layui.layer.alert("修改成功");
                        UrlManager.btn_back();
                        layui.table.reload("receiveAccount_table");
                    }

                }
            })
            return;
        }
        if ($("#add_Account_type").val() != 3)  {
            if (Common.isNil($("#name").val())) {
                layui.layer.alert("请输入外链名称")
                return;
            }
            if (Common.isNil($("#url").val())) {
                layui.layer.alert("请输入外链地址");
                return;
            }
            if (Common.isNil($("#receiveCodeImg_update").html())) {
                layui.layer.alert("请上传外链图标");
                return;
            }
            Common.invoke({
                url: request("/console/addUrl"),
                data: {
                    icon: Common.filterHtmlData($("#receiveCodeImg_update").html()),
                    type: Common.getValueForElement("#add_Account_type"),
                    name: Common.getValueForElement("#name"),
                    url: Common.getValueForElement("#url"),
                    status: $('input[name="status"]:checked').val()
                },
                success: function (result) {
                    if (result.resultCode == 1) {
                        layui.layer.alert("添加成功");
                        UrlManager.btn_back();
                        layui.table.reload("receiveAccount_table");
                    }
                }
            })
        }

    },
    deleteReceiveAccount: function (id) {
        layer.confirm('确定删除该外链？', {icon: 3, title: '提示信息'}, function (index) {
            Common.invoke({
                url: request("/console/deleteUrl"),
                data: {
                    id: id
                },
                success: function (result) {
                    if (result.resultCode == 1) {
                        layui.layer.alert("删除成功")
                        layui.table.reload("receiveAccount_table");
                    }
                }
            })
        })

    },
    updateReceiveAccount: function (data) {
        $("#receiveAccountList").hide();
        $("#AddreceiveAccount").show();
        $("#icon_tr").show();
        $("#url_tr").show();
        $("#status_tr").show();
        $("#updateReceiveAccountId").val(Common.filterHtmlData(data.id));
        $("#name").val(Common.filterHtmlData(data.name));
        $("#url").val(Common.filterHtmlData(data.url));
        $("#add_Account_type").val(Common.filterHtmlData(data.type));
        $("input[name=status][value=0]").prop("checked",data.status==0? true:false);
        $("input[name=status][value=1]").prop("checked",data.status==1? true:false);
    },
    btn_back: function () {
        $("#receiveAccountList").show();
        $("#AddreceiveAccount").hide();
    }
}